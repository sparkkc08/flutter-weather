import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:fltr_weather/models/DarkWeatherData.dart';
import 'package:fltr_weather/utils/utils.dart';
import 'package:geocoder/geocoder.dart';

class DayWeather extends StatelessWidget {

  final Currently today;
  final Address address;

  DayWeather({Key key, @required this.today, this.address}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        address != null ? Text('${address.locality}, ${address.countryName}',
          style: new TextStyle(color: Colors.white, fontSize: 32.0), textAlign: TextAlign.center,)
            : Container(),
        Text(today.summary, style: new TextStyle(color: Colors.white, fontSize: 24.0)),
        Text('${today.temperature.toString()} °C', style: new TextStyle(color: Colors.white)),
        Image.network(getImageUrl(today.icon)),
        Text(new DateFormat.yMMMd().format(today.time), style: new TextStyle(color: Colors.white)),
        Text(new DateFormat.Hm().format(today.time), style: new TextStyle(color: Colors.white)),
      ],
    );
  }
}