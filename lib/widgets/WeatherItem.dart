import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:fltr_weather/models/DarkWeatherData.dart';
import 'package:fltr_weather/utils/utils.dart';

class WeatherItem extends StatelessWidget {

  final Datum day;
  final count;
  final index;

  WeatherItem({Key key, @required this.day, this.count, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(index == 0 ? 8.0 : 4.0, 4.0, index == count - 1 ? 8.0 : 4.0, 4.0),
      child: Container(
        width: 150.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('${day.temperatureLow.toString()}...${day.temperatureHigh.toString()} °C', style: new TextStyle(color: Colors.black, fontSize: 24.0)),
              new Expanded(
                child: new Text(day.summary, style: new TextStyle(color: Colors.black),
                    textAlign: TextAlign.center),
              ),
              Image.network(getImageUrl(day.icon)),
              Text('${day.pressure.toString()} hPa, φ - ${(day.humidity * 100)?.round().toString()}%', style: new TextStyle(color: Colors.black)),
              Text(new DateFormat.yMMMd().format(day.time), style: new TextStyle(color: Colors.black)),
            ],
          ),
        ),
      )
    );
  }
}