String getImageUrl(String icon) {
  String image;
  switch(icon) {
    case 'clear-day':
      image = '01d';
      break;
    case 'clear-night':
      image = '01n';
      break;
    case 'rain':
      image = '09d';
      break;
    case 'snow':
      image = '13d';
      break;
    case 'sleet':
      image = '13d';
      break;
    case 'wind':
      image = '03d';
      break;
    case 'fog':
      image = '50d';
      break;
    case 'cloudy':
      image = '04d';
      break;
    case 'partly-cloudy-day':
      image = '02d';
      break;
    case 'partly-cloudy-night':
      image = '02n';
      break;
    default:
      image = '50d';
      break;
  }
  return 'https://openweathermap.org/img/w/$image.png';
}