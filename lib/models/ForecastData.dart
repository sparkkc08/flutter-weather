import 'package:fltr_weather/models/WeatherData.dart';

class ForecastData {
  final List list;

  ForecastData({this.list});

  factory ForecastData.fromJson(Map<String, dynamic> json) {
    List list = new List();

    for (dynamic e in json['list']) {
      WeatherData w = new WeatherData.fromJson(e);
      w.name = json['city']['name'];
      list.add(w);
    }

    return ForecastData(
      list: list,
    );
  }
}