// To parse this JSON data, do
//
//     final darkWeatherData = darkWeatherDataFromJson(jsonString);

import 'dart:convert';
import 'package:geocoder/geocoder.dart';

DarkWeatherData darkWeatherDataFromJson(String str) {
  final jsonData = json.decode(str);
  return DarkWeatherData.fromJson(jsonData);
}

String darkWeatherDataToJson(DarkWeatherData data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class DarkWeatherData {
  double latitude;
  double longitude;
  String timezone;
  Currently currently;
  Daily daily;
  List<Alert> alerts;
  Flags flags;
  Address address;

  DarkWeatherData({
    this.latitude,
    this.longitude,
    this.timezone,
    this.currently,
    this.daily,
    this.alerts,
    this.flags,
  });

  factory DarkWeatherData.fromJson(Map<String, dynamic> json) => new DarkWeatherData(
    latitude: json["latitude"]?.toDouble(),
    longitude: json["longitude"]?.toDouble(),
    timezone: json["timezone"],
    currently: Currently.fromJson(json["currently"]),
    daily: Daily.fromJson(json["daily"]),
    alerts: null/*new List<Alert>.from(json["alerts"]?.map((x) => Alert.fromJson(x)))*/,
    flags: Flags.fromJson(json["flags"]),
  );

  Map<String, dynamic> toJson() => {
    "latitude": latitude,
    "longitude": longitude,
    "timezone": timezone,
    "currently": currently?.toJson(),
    "daily": daily?.toJson(),
    "alerts": new List<dynamic>.from(alerts?.map((x) => x.toJson())),
    "flags": flags?.toJson(),
  };
}

class Alert {
  String title;
  int time;
  int expires;
  String description;
  String uri;

  Alert({
    this.title,
    this.time,
    this.expires,
    this.description,
    this.uri,
  });

  factory Alert.fromJson(Map<String, dynamic> json) => new Alert(
    title: json["title"],
    time: json["time"],
    expires: json["expires"],
    description: json["description"],
    uri: json["uri"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "time": time,
    "expires": expires,
    "description": description,
    "uri": uri,
  };
}

class Currently {
  DateTime time;
  String summary;
  String icon;
  int nearestStormDistance;
  double precipIntensity;
  double precipIntensityError;
  double precipProbability;
  String precipType;
  int temperature;
  double apparentTemperature;
  double dewPoint;
  double humidity;
  double pressure;
  double windSpeed;
  double windGust;
  int windBearing;
  double cloudCover;
  int uvIndex;
  double visibility;
  double ozone;

  Currently({
    this.time,
    this.summary,
    this.icon,
    this.nearestStormDistance,
    this.precipIntensity,
    this.precipIntensityError,
    this.precipProbability,
    this.precipType,
    this.temperature,
    this.apparentTemperature,
    this.dewPoint,
    this.humidity,
    this.pressure,
    this.windSpeed,
    this.windGust,
    this.windBearing,
    this.cloudCover,
    this.uvIndex,
    this.visibility,
    this.ozone,
  });

  factory Currently.fromJson(Map<String, dynamic> json) => new Currently(
    time: new DateTime.fromMillisecondsSinceEpoch(json["time"] * 1000, isUtc: false),
    summary: json["summary"],
    icon: json["icon"],
    nearestStormDistance: json["nearestStormDistance"],
    precipIntensity: json["precipIntensity"]?.toDouble(),
    precipIntensityError: json["precipIntensityError"]?.toDouble(),
    precipProbability: json["precipProbability"]?.toDouble(),
    precipType: json["precipType"],
    temperature: json["temperature"]?.toDouble()?.round(),
    apparentTemperature: json["apparentTemperature"]?.toDouble(),
    dewPoint: json["dewPoint"]?.toDouble(),
    humidity: json["humidity"]?.toDouble(),
    pressure: json["pressure"]?.toDouble(),
    windSpeed: json["windSpeed"]?.toDouble(),
    windGust: json["windGust"]?.toDouble(),
    windBearing: json["windBearing"],
    cloudCover: json["cloudCover"]?.toDouble(),
    uvIndex: json["uvIndex"],
    visibility: json["visibility"]?.toDouble(),
    ozone: json["ozone"]?.toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "time": time,
    "summary": summary,
    "icon": icon,
    "nearestStormDistance": nearestStormDistance,
    "precipIntensity": precipIntensity,
    "precipIntensityError": precipIntensityError,
    "precipProbability": precipProbability,
    "precipType": precipType,
    "temperature": temperature,
    "apparentTemperature": apparentTemperature,
    "dewPoint": dewPoint,
    "humidity": humidity,
    "pressure": pressure,
    "windSpeed": windSpeed,
    "windGust": windGust,
    "windBearing": windBearing,
    "cloudCover": cloudCover,
    "uvIndex": uvIndex,
    "visibility": visibility,
    "ozone": ozone,
  };
}

class Daily {
  String summary;
  String icon;
  List<Datum> data;

  Daily({
    this.summary,
    this.icon,
    this.data,
  });

  factory Daily.fromJson(Map<String, dynamic> json) => new Daily(
    summary: json["summary"],
    icon: json["icon"],
    data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "summary": summary,
    "icon": icon,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  DateTime time;
  String summary;
  String icon;
  int sunriseTime;
  int sunsetTime;
  double moonPhase;
  double precipIntensity;
  double precipIntensityMax;
  int precipIntensityMaxTime;
  double precipProbability;
  String precipType;
  int temperatureHigh;
  int temperatureHighTime;
  int temperatureLow;
  int temperatureLowTime;
  int apparentTemperatureHigh;
  int apparentTemperatureHighTime;
  int apparentTemperatureLow;
  int apparentTemperatureLowTime;
  double dewPoint;
  double humidity;
  int pressure;
  double windSpeed;
  double windGust;
  int windGustTime;
  int windBearing;
  double cloudCover;
  int uvIndex;
  int uvIndexTime;
  double visibility;
  double ozone;
  double temperatureMin;
  int temperatureMinTime;
  double temperatureMax;
  int temperatureMaxTime;
  double apparentTemperatureMin;
  int apparentTemperatureMinTime;
  double apparentTemperatureMax;
  int apparentTemperatureMaxTime;

  Datum({
    this.time,
    this.summary,
    this.icon,
    this.sunriseTime,
    this.sunsetTime,
    this.moonPhase,
    this.precipIntensity,
    this.precipIntensityMax,
    this.precipIntensityMaxTime,
    this.precipProbability,
    this.precipType,
    this.temperatureHigh,
    this.temperatureHighTime,
    this.temperatureLow,
    this.temperatureLowTime,
    this.apparentTemperatureHigh,
    this.apparentTemperatureHighTime,
    this.apparentTemperatureLow,
    this.apparentTemperatureLowTime,
    this.dewPoint,
    this.humidity,
    this.pressure,
    this.windSpeed,
    this.windGust,
    this.windGustTime,
    this.windBearing,
    this.cloudCover,
    this.uvIndex,
    this.uvIndexTime,
    this.visibility,
    this.ozone,
    this.temperatureMin,
    this.temperatureMinTime,
    this.temperatureMax,
    this.temperatureMaxTime,
    this.apparentTemperatureMin,
    this.apparentTemperatureMinTime,
    this.apparentTemperatureMax,
    this.apparentTemperatureMaxTime,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
    time: new DateTime.fromMillisecondsSinceEpoch(json["time"] * 1000, isUtc: false),
    summary: json["summary"],
    icon: json["icon"],
    sunriseTime: json["sunriseTime"],
    sunsetTime: json["sunsetTime"],
    moonPhase: json["moonPhase"]?.toDouble(),
    precipIntensity: json["precipIntensity"]?.toDouble(),
    precipIntensityMax: json["precipIntensityMax"]?.toDouble(),
    precipIntensityMaxTime: json["precipIntensityMaxTime"],
    precipProbability: json["precipProbability"]?.toDouble(),
    precipType: json["precipType"],
    temperatureHigh: json["temperatureHigh"]?.toDouble()?.round(),
    temperatureHighTime: json["temperatureHighTime"],
    temperatureLow: json["temperatureLow"]?.toDouble()?.round(),
    temperatureLowTime: json["temperatureLowTime"],
    apparentTemperatureHigh: json["apparentTemperatureHigh"]?.toDouble()?.round(),
    apparentTemperatureHighTime: json["apparentTemperatureHighTime"],
    apparentTemperatureLow: json["apparentTemperatureLow"]?.toDouble()?.round(),
    apparentTemperatureLowTime: json["apparentTemperatureLowTime"],
    dewPoint: json["dewPoint"]?.toDouble(),
    humidity: json["humidity"]?.toDouble(),
    pressure: json["pressure"]?.toDouble()?.round(),
    windSpeed: json["windSpeed"]?.toDouble(),
    windGust: json["windGust"]?.toDouble(),
    windGustTime: json["windGustTime"],
    windBearing: json["windBearing"],
    cloudCover: json["cloudCover"]?.toDouble(),
    uvIndex: json["uvIndex"],
    uvIndexTime: json["uvIndexTime"],
    visibility: json["visibility"]?.toDouble(),
    ozone: json["ozone"]?.toDouble(),
    temperatureMin: json["temperatureMin"]?.toDouble(),
    temperatureMinTime: json["temperatureMinTime"],
    temperatureMax: json["temperatureMax"]?.toDouble(),
    temperatureMaxTime: json["temperatureMaxTime"],
    apparentTemperatureMin: json["apparentTemperatureMin"]?.toDouble(),
    apparentTemperatureMinTime: json["apparentTemperatureMinTime"],
    apparentTemperatureMax: json["apparentTemperatureMax"]?.toDouble(),
    apparentTemperatureMaxTime: json["apparentTemperatureMaxTime"],
  );

  Map<String, dynamic> toJson() => {
    "time": time,
    "summary": summary,
    "icon": icon,
    "sunriseTime": sunriseTime,
    "sunsetTime": sunsetTime,
    "moonPhase": moonPhase,
    "precipIntensity": precipIntensity,
    "precipIntensityMax": precipIntensityMax,
    "precipIntensityMaxTime": precipIntensityMaxTime,
    "precipProbability": precipProbability,
    "precipType": precipType,
    "temperatureHigh": temperatureHigh,
    "temperatureHighTime": temperatureHighTime,
    "temperatureLow": temperatureLow,
    "temperatureLowTime": temperatureLowTime,
    "apparentTemperatureHigh": apparentTemperatureHigh,
    "apparentTemperatureHighTime": apparentTemperatureHighTime,
    "apparentTemperatureLow": apparentTemperatureLow,
    "apparentTemperatureLowTime": apparentTemperatureLowTime,
    "dewPoint": dewPoint,
    "humidity": humidity,
    "pressure": pressure,
    "windSpeed": windSpeed,
    "windGust": windGust,
    "windGustTime": windGustTime,
    "windBearing": windBearing,
    "cloudCover": cloudCover,
    "uvIndex": uvIndex,
    "uvIndexTime": uvIndexTime,
    "visibility": visibility,
    "ozone": ozone,
    "temperatureMin": temperatureMin,
    "temperatureMinTime": temperatureMinTime,
    "temperatureMax": temperatureMax,
    "temperatureMaxTime": temperatureMaxTime,
    "apparentTemperatureMin": apparentTemperatureMin,
    "apparentTemperatureMinTime": apparentTemperatureMinTime,
    "apparentTemperatureMax": apparentTemperatureMax,
    "apparentTemperatureMaxTime": apparentTemperatureMaxTime,
  };
}

class Flags {
  String units;

  Flags({
    this.units,
  });

  factory Flags.fromJson(Map<String, dynamic> json) => new Flags(
    units: json["units"],
  );

  Map<String, dynamic> toJson() => {
    "units": units,
  };
}
