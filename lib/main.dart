import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';

import 'package:fltr_weather/widgets/Weather.dart';
import 'package:fltr_weather/widgets/WeatherItem.dart';
import 'package:fltr_weather/models/DarkWeatherData.dart';
import 'package:fltr_weather/utils/constants.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Weather',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Weather'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isLoading = false;
  DarkWeatherData darkWeatherData;
  Location location = new Location();
  String error;

  @override
  void initState() {
    super.initState();
    loadWeather();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),
      ),
      backgroundColor: Colors.lightBlueAccent,
      body: new Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding (
                      padding: EdgeInsets.all(8.0),
                      child: darkWeatherData != null ? DayWeather(
                          today: darkWeatherData.currently,
                          address: darkWeatherData.address)
                      : Container(),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: isLoading ? CircularProgressIndicator(
                        strokeWidth: 4.0,
                        valueColor: new AlwaysStoppedAnimation(Colors.white),
                      ) : IconButton(
                          icon: new Icon(Icons.refresh),
                          tooltip: 'Refresh',
                          onPressed: loadWeather,
                          color: Colors.white
                      ),
                    )
                  ],
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Container(
                    height: 200.0,
                    child: darkWeatherData != null ? ListView.builder(
                      itemCount: darkWeatherData.daily.data.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => WeatherItem(
                        day: darkWeatherData.daily.data.elementAt(index),
                        count: darkWeatherData.daily.data.length,
                        index: index,
                      ),
                    ) : Container(),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  loadWeather() async {
    setState(() {
      isLoading = true;
    });

    Map<String, double> currentLocation;
    try {
      currentLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'Permission denied - please ask the user to enable it from the app settings';
      }

      currentLocation = null;
    }

    if (currentLocation != null) {
      final lat = currentLocation['latitude'];
      final lon = currentLocation['longitude'];
      final coordinates = new Coordinates(lat, lon);

      final url = "$DS_API_URL${lat.toString()},${lon.toString()}?exclude=minutely,hourly&units=si";
      final weatherResponse = await http.get(url);
      final addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

      if (weatherResponse.statusCode == 200) {

        return setState(() {
          print(jsonDecode(weatherResponse.body));
          darkWeatherData = new DarkWeatherData.fromJson(jsonDecode(weatherResponse.body));
          darkWeatherData.address = addresses.length > 0 ? addresses.first : null;
          isLoading = false;
        });
      }
    }

    setState(() {
      isLoading = false;
    });
  }
}
